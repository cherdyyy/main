### Local development
RUN COMMAND - `docker-compose -f docker-compose.yml -f docker-compose.local.yml up -d`

RUN AND BUILD COMMAND - `docker-compose -f docker-compose.yml -f docker-compose.local.yml up -d --build`

### RUN PROD CONTAINERS
RUN COMMAND - `docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d`

RUN AND BUILD COMMAND - `docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d --build`
