FROM node:latest

EXPOSE 4200

WORKDIR /app

COPY ./services/node/package*.json ./

RUN npm install

CMD ["npm", "run", "frontend-admin:serve"]
