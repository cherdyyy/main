FROM node:latest

EXPOSE 3000

WORKDIR /app

COPY ./services/node/package*.json ./

RUN npm install

CMD ["npm", "run", "backend:serve"]
