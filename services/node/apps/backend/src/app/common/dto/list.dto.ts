import { IsNotEmpty, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';
import { ListProp } from '@backend/app/common/interfaces/list.interface';

export const getListDto = <T>(item: () => void): ListProp<T> => {
	class ListDto implements ListProp<T> {
		@IsNotEmpty()
		@ValidateNested({ each: true })
		@Type(() => item)
		list: T[];
	}
	return (ListDto as unknown) as ListProp<T>;
};
