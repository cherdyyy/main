import { Global, Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { JWT_REGISTER } from '@backend/app/api/auth/configs/jwt-register.config';
import { JwtStrategy } from '@backend/app/api/auth/strategies/jwt.strategy';

@Global()
@Module({
  providers: [
    AuthService,
    JwtStrategy
  ],
  imports: [
    PassportModule,
    JwtModule.registerAsync(JWT_REGISTER),
  ],
  controllers: [
    AuthController
  ],
  exports: [
    AuthService
  ]
})
export class AuthModule {
}
