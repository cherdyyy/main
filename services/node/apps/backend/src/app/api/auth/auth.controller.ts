import { Body, Controller, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { AuthService } from '@backend/app/api/auth/auth.service';
import { SignInDto } from '@backend/app/api/auth/dto/sign-in.dto';
import { TokenModel } from '@backend/app/api/auth/models/token.model';
import { SignUpDto } from '@backend/app/api/auth/dto/sign-up.dto';

@ApiTags('Authorization')
@Controller('auth')
export class AuthController {
	constructor(private authService: AuthService) {}

	@Post('sign-in')
	signIn(@Body() body: SignInDto): Promise<TokenModel> {
		return this.authService.signIn(body);
	}

	@Post('sign-up')
	signUp(@Body() body: SignUpDto): Promise<TokenModel> {
		return this.authService.signUp(body);
	}
}
