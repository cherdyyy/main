import { applyDecorators, UseGuards } from '@nestjs/common';
import { ApiHeader } from '@nestjs/swagger';
import { JwtAuthGuard } from '@backend/app/api/auth/guards/jwt-auth.guard';

export function Auth() {
	return applyDecorators(
		UseGuards(JwtAuthGuard),
		ApiHeader({
			name: 'Authorization',
		}),
	);
}
