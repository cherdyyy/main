export enum AuthResponseTextEnum {
	USER_EXIST = 'Such user is already exist!',
	LOGIN_NOT_FOUND = 'User with such login is not found!',
	INCORRECT_PASSWORD = 'User password is incorrect!',
}
