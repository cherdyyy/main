import { IsEmail, IsNotEmpty, IsString, MinLength } from 'class-validator';

export class SignUpDto {
	@IsString()
	@MinLength(6)
	password: string;

	@IsString()
	firstName: string;

	@IsNotEmpty()
	lastName: string;

	@IsEmail()
	@IsNotEmpty()
	email: string;
}
