import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AppConfigService } from '@backend/app/core/config/config.service';
import { UserRepository } from '@backend/app/api/user/database/user.repository';
import { ConfigKeysEnum } from '@backend/app/core/config/enums/config-keys.enum';
import { TokenContextModel } from '@backend/app/api/auth/models/token-context.model';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
	constructor(
		private config: AppConfigService,
		@InjectRepository(UserRepository)
		private usersRepository: UserRepository,
	) {
		super({
			jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
			ignoreExpiration: false,
			secretOrKey: config.get(ConfigKeysEnum.JWT_SECRET_KEY),
		});
	}

	async validate(payload: TokenContextModel) {
		return this.usersRepository.getFullUser(payload.id);
	}
}
