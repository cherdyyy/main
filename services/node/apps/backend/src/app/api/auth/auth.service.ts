import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { compare } from 'bcrypt';
import { InjectRepository } from '@nestjs/typeorm';
import { UserRepository } from '@backend/app/api/user/database/user.repository';
import { UserService } from '@backend/app/api/user/user.service';
import { TokenModel } from '@backend/app/api/auth/models/token.model';
import { SignUpDto } from '@backend/app/api/auth/dto/sign-up.dto';
import { AuthResponseTextEnum } from '@backend/app/api/auth/enums/response-text.enum';
import { TokenContextModel } from '@backend/app/api/auth/models/token-context.model';
import { SignInDto } from '@backend/app/api/auth/dto/sign-in.dto';

@Injectable()
export class AuthService {
  constructor(
    private jwtService: JwtService,
    @InjectRepository(UserRepository)
    private usersRepository: UserRepository,
    private userService: UserService,
  ) {
  }

  async signUp(body: SignUpDto): Promise<TokenModel> {
    const user = await this.usersRepository.findOne({
      email: body.email,
    });

    if (user) {
      throw new HttpException(AuthResponseTextEnum.USER_EXIST, HttpStatus.CONFLICT);
    }

    const newUser = await this.userService.createUser(body);

    return new TokenModel(
      await this.jwtService.signAsync({
        ...new TokenContextModel(newUser.id),
      }),
    );
  }

  async signIn(body: SignInDto): Promise<TokenModel> {
    const user = await this.usersRepository.findOne({
      email: body.email,
    });

    if (!user) {
      throw new HttpException(AuthResponseTextEnum.LOGIN_NOT_FOUND, HttpStatus.UNAUTHORIZED);
    }

    if (!(await compare(body.password, user.password))) {
      throw new HttpException(AuthResponseTextEnum.INCORRECT_PASSWORD, HttpStatus.UNAUTHORIZED);
    }

    return new TokenModel(
      await this.jwtService.signAsync({
        ...new TokenContextModel(user.id),
      }),
    );
  }
}
