import { BaseModel } from '@backend/app/common/models/base.model';
import { UserEntity } from '@backend/app/api/user/database/user.entity';

export class UserModel extends BaseModel {
  email: string;
  firstName: string;
  lastName: string;

  constructor(entity: Partial<UserEntity> = {}) {
    super(entity);
    this.firstName = entity.firstName;
    this.email = entity.email;
    this.lastName = entity.lastName;
  }
}
