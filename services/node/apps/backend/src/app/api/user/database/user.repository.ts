import { EntityRepository } from 'typeorm';
import { UserEntity } from '@backend/app/api/user/database/user.entity';
import { AppBaseRepository } from '@backend/app/common/database/repositories/base.repository';

@EntityRepository(UserEntity)
export class UserRepository extends AppBaseRepository<UserEntity> {
	async getFullUser(id: number): Promise<UserEntity> {
		return this.createQueryBuilder('user_entity')
			.where('user_entity.id = :id', { id })
			.getOne();
	}
}
