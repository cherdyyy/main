import { Entity, Column } from 'typeorm';
import { AppBaseEntity } from '@backend/app/common/database/entities/base.entity';
import { UserTypeEnum } from '@backend/app/api/user/enums/user-type.enum';

@Entity()
export class UserEntity extends AppBaseEntity {
	@Column()
	password: string;

	@Column()
	firstName: string;

	@Column()
	lastName: string;

	@Column()
	email: string;

	@Column('enum', {
		enum: UserTypeEnum,
		default: UserTypeEnum.Default,
	})
	type: UserTypeEnum;
}
