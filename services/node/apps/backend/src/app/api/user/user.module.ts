import { Global, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { UserRepository } from '@backend/app/api/user/database/user.repository';

@Global()
@Module({
  providers: [
    UserService
  ],
  imports: [
    TypeOrmModule.forFeature([UserRepository]),
  ],
  exports: [
    UserService,
    TypeOrmModule
  ],
  controllers: [
    UserController
  ]
})
export class UserModule {}
