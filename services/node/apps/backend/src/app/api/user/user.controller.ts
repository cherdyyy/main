import { Controller, Get } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { UserService } from '@backend/app/api/user/user.service';
import { User } from '@backend/app/common/decorators/user.decorator';
import { Auth } from '@backend/app/api/auth/decorators/auth.decorator';
import { UserModel } from '@backend/app/api/user/models/user.model';
import { UserEntity } from '@backend/app/api/user/database/user.entity';

@ApiTags('User')
@Auth()
@Controller('user')
export class UserController {
	constructor(private userService: UserService) {}

	@Get()
	getUser(@User() user: UserEntity): UserModel {
		return new UserModel(user);
	}
}
