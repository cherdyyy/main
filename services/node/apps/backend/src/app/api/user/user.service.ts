import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { hash } from 'bcrypt';
import { UserRepository } from '@backend/app/api/user/database/user.repository';
import { UserEntity } from '@backend/app/api/user/database/user.entity';

@Injectable()
export class UserService {
	constructor(
		@InjectRepository(UserRepository)
		private usersRepository: UserRepository,
	) {}

	async createUser(body: Partial<UserEntity>): Promise<UserEntity> {
		return this.usersRepository.insertAndReturnOne({
			...body,
			password: await hash(body.password, 10),
		});
	}
}
