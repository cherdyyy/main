import { Module } from '@nestjs/common';
import { UserModule } from './user/user.module';
import { AuthModule } from '@backend/app/api/auth/auth.module';

const FEATURES = [
  AuthModule,
  UserModule,
];

@Module({
  imports: [
    ...FEATURES,
  ],
  exports: [
    ...FEATURES
  ]
})
export class ApiModule {}
