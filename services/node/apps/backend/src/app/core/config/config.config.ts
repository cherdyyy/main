import { ConfigModuleOptions } from '@nestjs/config/dist/interfaces';
import { configValidator } from '@backend/app/core/config/config.validator';

export const configSettings: ConfigModuleOptions = {
	validationSchema: configValidator,
	validationOptions: {
		abortEarly: true,
	},
};
