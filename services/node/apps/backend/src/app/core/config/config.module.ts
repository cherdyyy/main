import { Global, Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { configSettings } from '@backend/app/core/config/config.config';
import { AppConfigService } from '@backend/app/core/config/config.service';

@Global()
@Module({
  imports: [
    ConfigModule.forRoot(configSettings),
  ],
  providers: [
    AppConfigService
  ],
  exports: [
    AppConfigService
  ]
})
export class AppConfigModule {}
