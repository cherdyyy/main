import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DATABASE_SETTINGS } from '@backend/app/core/database/database.config';

@Module({
  imports: [
    TypeOrmModule.forRootAsync(DATABASE_SETTINGS),
  ]
})
export class DatabaseModule {}
