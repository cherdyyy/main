import { TypeOrmModuleAsyncOptions, TypeOrmModuleOptions } from '@nestjs/typeorm';
import { getMetadataArgsStorage } from 'typeorm';
import { AppConfigService } from '@backend/app/core/config/config.service';
import { ConfigKeysEnum } from '@backend/app/core/config/enums/config-keys.enum';

export const DATABASE_SETTINGS: TypeOrmModuleAsyncOptions = {
	useFactory: async (configService: AppConfigService): Promise<TypeOrmModuleOptions> => ({
		type: 'postgres',
		host: configService.get(ConfigKeysEnum.DATABASE_HOST),
		port: configService.get<number>(ConfigKeysEnum.DATABASE_PORT),
		username: configService.get(ConfigKeysEnum.DATABASE_USER),
		password: configService.get(ConfigKeysEnum.DATABASE_PASSWORD),
		database: configService.get(ConfigKeysEnum.DATABASE_NAME),
		synchronize: true,
		entities: getMetadataArgsStorage().tables.map((tbl) => tbl.target),
	}),
	inject: [AppConfigService],
};
