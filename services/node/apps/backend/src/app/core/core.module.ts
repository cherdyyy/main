import { Global, Module } from '@nestjs/common';
import { DatabaseModule } from './database/database.module';
import { AppConfigModule } from '@backend/app/core/config/config.module';

@Global()
@Module({
  imports: [
    AppConfigModule,
    DatabaseModule,
  ],
  exports: [
    AppConfigModule,
  ]
})
export class CoreModule {

}
