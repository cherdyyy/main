import { Module } from '@nestjs/common';
import { CoreModule } from '@backend/app/core/core.module';
import { ApiModule } from '@backend/app/api/api.module';

@Module({
  imports: [
    CoreModule,
    ApiModule
  ]
})
export class AppModule {
}
