import { NestFactory } from '@nestjs/core';

import { AppModule } from './app/app.module';
import { swaggerBootstrap } from '@backend/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  await swaggerBootstrap(app);
  app.enableCors();
  await app.listen(3000);
}

bootstrap();
