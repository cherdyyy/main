import { Component } from '@angular/core';

@Component({
  selector: 'node-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'frontend-admin';
}
